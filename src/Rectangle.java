public class Rectangle {
    int sideA;
    int sideB;
    Point topLeft;

    public Rectangle(int sideA, int sideB, Point topLeft) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.topLeft = topLeft;
    }

    public int area() {
        return sideA * sideB;
    }

    public int perimeter() {
        return (sideA + sideB) * 2;
    }

    public int[][] Corners() {
        int[][] corners = new int [4][2];
        corners[0][0] = topLeft.xCoord;
        corners[0][1] = topLeft.yCoord;
        corners[1][0] = sideA;
        corners[1][1] = topLeft.yCoord;
        corners[2][0] = 0;
        corners[2][1] = 0;
        corners[3][0] = sideA;
        corners[3][1] = 0;


        return corners;
    }


}

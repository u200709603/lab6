import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Point topLeft = new Point(0, 8);
        Rectangle rectangle = new Rectangle(1, 8, topLeft);
        System.out.println("Area of Rectangle: " + rectangle.area());
        System.out.println("Perimeter of Rectangle: " + rectangle.perimeter());
        System.out.println("Corners of Rectangle: " + Arrays.deepToString(rectangle.Corners()));
        Point center = new Point(0, 0);
        Circle circle = new Circle(10, center);
        System.out.println("Circle's area: " + circle.area());
        System.out.println("Circle's perimeter: " + circle.perimeter());
        Point center2 = new Point(10, 10);
        Circle circle2 = new Circle(5, center2);
        if (circle.intersect(circle2)) System.out.println("Two circles intersect");
        else System.out.println("Two circle is not intersect");


    }
}

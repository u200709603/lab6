
public class Circle {
    int radius;
    Point center;

    public Circle(int radius, Point center) {
        this.radius = radius;
        this.center = center;
    }

    public double area(){
        return Math.PI*radius*radius;
    }
    public double perimeter(){
        return 2*radius*Math.PI;
    }
    public boolean intersect(Circle circle){
        double bD;
        double totalRadius= radius+ circle.radius;
        //betweenDistance
        bD=Math.sqrt(Math.pow(Math.abs(center.xCoord-circle.center.xCoord),2)+Math.pow(Math.abs(center.yCoord-circle.center.yCoord),2));
        return bD <= totalRadius;
    }

}
